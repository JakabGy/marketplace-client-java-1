package com.atlassian.marketplace.client.api;

import java.net.URI;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Abstraction of the URI of a set of query results that can be pulled from the server.
 * @see Page
 */
public final class PageReference<T>
{
    private final URI uri;
    private final QueryBounds bounds;
    private final PageReader<T> reader;
    
    public PageReference(URI uri, QueryBounds bounds, PageReader<T> reader)
    {
        this.uri = checkNotNull(uri);
        this.bounds = checkNotNull(bounds);
        this.reader = checkNotNull(reader);
    }
    
    /**
     * The REST URI of the result page.
     */
    public URI getUri()
    {
        return uri;
    }
    
    /**
     * The starting offset and limit of the result page.
     */
    public QueryBounds getBounds()
    {
        return bounds;
    }
    
    public PageReader<T> getReader()
    {
        return reader;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object other)
    {
        if (other instanceof PageReference)
        {
            PageReference<T> p = (PageReference<T>) other;
            return uri.equals(p.uri) && bounds.equals(p.bounds); 
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return uri.hashCode() + bounds.hashCode();
    }
}
