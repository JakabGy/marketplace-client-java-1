package com.atlassian.marketplace.client.encoding;

/**
 * Indicates that an object returned by the server had some JSON parsing error that
 * could not be described by one of the other {@link SchemaViolation} subclasses.
 * @since 2.0.0
 */
public class MalformedJson extends SchemaViolation
{
    private final String message;
    
    public MalformedJson(Class<?> schemaClass, String message)
    {
        super(schemaClass);
        this.message = message;
    }
    
    @Override
    public String getMessage()
    {
        return "Malformed JSON for " + getSchemaClass().getSimpleName() + ": " + message;
    }
}
