package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.AddonCategoryId;

import java.net.URI;

/**
 * Describes a category that add-ons can be listed in.
 * @since 2.0.0
 */
public final class AddonCategorySummary implements Entity
{
    Links _links;
    @RequiredLink(rel = "self") URI selfUri;
    
    String name;
    
    public Links getLinks()
    {
        return _links;
    }

    public URI getSelfUri()
    {
        return selfUri;
    }
    
    /**
     * The unique resource identifier for the category.
     * @see AddonBase#getCategoryIds()
     * @see AddonVersionBase#getFunctionalCategoryIds()
     * @see ModelBuilders.AddonBuilder#categories(Iterable)
     */
    public AddonCategoryId getId()
    {
        return AddonCategoryId.fromUri(selfUri);
    }
    
    /**
     * The category name.
     */
    public String getName()
    {
        return name;
    }
}
