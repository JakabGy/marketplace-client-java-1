package com.atlassian.marketplace.client.util;

import com.google.common.collect.ImmutableList;
import io.atlassian.fugue.Option;

import java.util.Optional;

/**
 * These methods exist to service the deprecation of fugue Option.
 * Therefore they should be removed when fugue option is no longer present in the code base
 */
public class Convert {

    /**
     * @deprecated Don't use fugue option in the first place
     */
    @Deprecated
    public static <T> Optional<T> toOptional(Option<T> op)
    {
        return op.map(Optional::of).getOrElse(Optional.empty());
    }

    /**
     * @deprecated Don't use fugue option in the first place
     */
    @Deprecated
    public static <T> Option<T> fugueOption(Optional<T> op)
    {

        return op.map(Option::some).orElseGet(Option::none);
    }

    public static <T> Iterable<T> iterableOf(Optional<T> op) {
        return op.map(ImmutableList::of).orElseGet(ImmutableList::of);
    }

}
