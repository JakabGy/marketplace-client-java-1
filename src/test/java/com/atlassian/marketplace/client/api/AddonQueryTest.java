package com.atlassian.marketplace.client.api;

import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Optional;

import static com.atlassian.marketplace.client.api.AddonQuery.any;
import static com.atlassian.marketplace.client.api.AddonQuery.builder;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.api.Cost.ALL_PAID;
import static com.atlassian.marketplace.client.api.HostingType.CLOUD;
import static com.atlassian.marketplace.client.api.HostingType.DATA_CENTER;
import static com.atlassian.marketplace.client.api.HostingType.SERVER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class AddonQueryTest
{
    @Test
    public void defaultQueryIsSameAsAny()
    {
        assertThat(builder().build(), equalTo(any()));
    }
    
    @Test
    public void defaultQueryHasNoAccessToken()
    {
        assertThat(any().safeGetAccessToken(), equalTo(Optional.empty()));
    }
    
    @Test
    public void accessTokenCanBeSet()
    {
        assertThat(builder().accessToken(Optional.of("x")).build().safeGetAccessToken(), equalTo(Optional.of("x")));
    }
    
    @Test
    public void defaultQueryHasNoApplication()
    {
        assertThat(any().safeGetApplication(), equalTo(Optional.empty()));
    }
    
    @Test
    public void applicationCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().application(Optional.of(JIRA)).build();
        assertThat(q.safeGetApplication(), equalTo(Optional.of(JIRA)));
    }
    
    @Test
    public void defaultQueryHasNoAppBuildNumber()
    {
        assertThat(any().safeGetAppBuildNumber(), equalTo(Optional.empty()));
    }
    
    @Test
    public void appBuildNumberCanBeSet()
    {
        AddonQuery q = builder().appBuildNumber(Optional.of(1000)).build();
        assertThat(q.safeGetAppBuildNumber(), equalTo(Optional.of(1000)));
    }

    @Test
    public void defaultQueryHasNoCategories()
    {
        assertThat(any().getCategoryNames(), Matchers.emptyIterable());
    }
    
    @Test
    public void categoriesCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().categoryNames(ImmutableList.of("a", "b")).build();
        assertThat(q.getCategoryNames(), Matchers.contains("a", "b"));
    }

    @Test
    public void defaultQueryHasNoCost()
    {
        assertThat(any().safeGetCost(), equalTo(Optional.empty()));
    }
    
    @Test
    public void costCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().cost(Optional.of(ALL_PAID)).build();
        assertThat(q.safeGetCost(), equalTo(Optional.of(ALL_PAID)));
    }

    @Test
    public void defaultQueryHasNoHosting()
    {
        assertThat(any().safeGetHosting(), equalTo(Optional.empty()));
    }

    @Test
    public void defaultQueryHasNoHostingValues()
    {
        assertThat(any().safeGetHosting(), equalTo(Optional.empty()));
    }

    @Test
    public void hostingCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().hosting(Optional.of(CLOUD)).build();
        assertThat(q.safeGetHosting(), equalTo(Optional.of(CLOUD)));
    }
    
    @Test
    public void multipleHostingCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().hosting(ImmutableList.of(DATA_CENTER, SERVER)).build();
        assertThat(q.getHostings(), Matchers.contains(DATA_CENTER, SERVER));
    }

    @Test
    public void defaultQueryHasNoSearchText()
    {
        assertThat(any().safeGetSearchText(), equalTo(Optional.empty()));
    }
    
    @Test
    public void searchTextCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().searchText(Optional.of("yo")).build();
        assertThat(q.safeGetSearchText(), equalTo(Optional.of("yo")));
    }
    
    @Test
    public void defaultQueryHasWithVersionFalse()
    {
        assertThat(any().isWithVersion(), equalTo(false));
    }
    
    @Test
    public void withVersionCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().withVersion(true).build();
        assertThat(q.isWithVersion(), equalTo(true));
    }

    @Test
    public void defaultQueryHasDefaultBounds()
    {
        assertThat(any().getBounds(), equalTo(QueryBounds.defaultBounds()));
    }

    @Test
    public void boundsCanBeSet()
    {
        QueryBounds b = QueryBounds.offset(2).withLimit(Optional.of(3));
        AddonQuery q = AddonQuery.builder().bounds(b).build();
        assertThat(q.getBounds(), equalTo(b));
    }
}
