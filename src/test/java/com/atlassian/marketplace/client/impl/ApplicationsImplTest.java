package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.util.UriBuilder;
import org.junit.Test;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.marketplace.client.TestObjects.HOST_BASE;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.impl.ClientTester.FAKE_APPLICATIONS_PATH;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;

public class ApplicationsImplTest extends ApiImplTestBase implements ApplicationsImplTestBase
{
    @Test
    public void getByKeyUsesApplicationsResource() throws Exception
    {
        setupAppByKeyResource(appByKeyUri(JIRA));
        tester.client.applications().safeGetByKey(JIRA);

        verify(tester.httpTransport).get(URI.create(HOST_BASE + FAKE_APPLICATIONS_PATH + "?limit=0"));
    }

    @Test
    public void getByKeyUsesAppByKeyResource() throws Exception
    {
        setupAppByKeyResource(appByKeyUri(JIRA));

        assertThat(tester.client.applications().safeGetByKey(JIRA), equalTo(Optional.of(APP_REP)));
    }

    protected UriBuilder appsApi()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_APPLICATIONS_PATH);
    }
    
    protected UriBuilder appsApiZeroLengthQuery()
    {
        return appsApi().queryParam("limit", 0);
    }

    protected UriBuilder appByKeyUri(ApplicationKey key)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_APP_BY_KEY_PATH + key.getKey());
    }

    protected void setupAppsResource(UriBuilder appsUri) throws Exception
    {
        setupAppsResource(appsUri, APPS_REP);
    }
    
    protected void setupAppsResource(UriBuilder appsUri, InternalModel.Applications rep) throws Exception
    {
        tester.mockResource(appsUri.build(), rep);
    }
    
    protected void setupAppByKeyResource(UriBuilder uri) throws Exception
    {
        setupAppsResource(appsApiZeroLengthQuery());
        tester.mockResource(uri.build(), APP_REP);
    }
}
